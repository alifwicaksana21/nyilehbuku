<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $categories = ['Fiksi', 'Pendidikan', 'Regilius', 'Anak-anak', 'Fashion'];
        $books = collect([
            'category_id' => [
                '0' => 2, '1' => 1
            ],
            'title' => [
                '0' => 'Filosofi Teras', '1' => 'Filosofi Kopi'
            ],
            'description' => [
                '0' => 'Buku tentang sebuah filosofi', '1' => 'Buku tentang cerita penjual kopi',
            ],
            'cover' => [
                '0' => 'www.google.com', '1' => 'www.google.com',
            ],
        ]);
        $books = [
            [
                'category_id' => 2,
                'title' => 'filosofi Teras',
                'description' => 'buku tentang filosofi romawi',
                'cover' => 'www.google.com',
                'location_id' => 1
            ],
            [
                'category_id' => 1,
                'title' => 'filosofi Kopi',
                'description' => 'buku tentang penjual kopi',
                'cover' => 'www.google.com',
                'location_id' => 3
            ],
            [
                'category_id' => 3,
                'title' => 'sifat sholat nabi',
                'description' => 'buku tentang tata cara sholat nabi',
                'cover' => 'www.google.com',
                'location_id' => 2
            ],
        ];

        DB::transaction(function () use ($categories, $books) {
            DB::table('users')->insert([
                'name' => 'Admin',
                'email' => 'admin' . '@gmail.com',
                'password' => bcrypt('12345678'),
                'is_admin' => true,
            ]);

            DB::table('users')->insert([
                'name' => 'Alif',
                'email' => 'alif' . '@gmail.com',
                'password' => bcrypt('12345678'),
                'is_admin' => false,
            ]);

            DB::table('locations')->insert([
                'floor' => 'Lantai 1'
            ]);
            DB::table('locations')->insert([
                'floor' => 'Lantai 2'
            ]);
            DB::table('locations')->insert([
                'floor' => 'Lantai 3'
            ]);

            foreach ($categories as $category) {
                DB::table('categories')->insert([
                    'category' => $category
                ]);
            }

            foreach ($books as $book) {
                DB::table('books')->insert([
                    'category_id' => $book['category_id'],
                    'title' => $book['title'],
                    'description' => $book['description'],
                    'cover' => $book['cover'],
                    'location_id' => $book['location_id']
                ]);
            }
        });
    }
}

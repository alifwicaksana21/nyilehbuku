<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $fields = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email|email',
            'password' => 'required|string|confirmed'
        ]);

        $user = User::create([
            'name' => $fields['name'],
            'email' => $fields['email'],
            'password' => bcrypt($fields['password']),
        ]);

        // $user = DB::table('users')->insert([
        //     'name' => $fields['name'],
        //     'email' => $fields['email'],
        //     'password' => bcrypt($fields['password']),
        // ]);
        // $user = User::where('email', $fields['email']);
        // $user = DB::table('users')->where('email', $fields['email'])->first();
        $token = $user->createToken('myapptoken')->plainTextToken;
        $user = User::find($user->id);
        $response = [
            'user' => $user,
            'token' => $token
        ];
        return $response;
    }

    public function login(Request $request)
    {
        $fields = $request->validate([
            'email' => 'required|string|exists:users,email',
            'password' => 'required|string'
        ]);

        $user = User::where('email', $fields['email'])->first();

        if (!Hash::check($fields['password'], $user->password)) {
            return response([
                'msg' => 'Password atau Email tidak cocok'
            ], 401);
        }

        $token = $user->createToken('myapptoken')->plainTextToken;
        $user = User::find($user->id);
        $response = [
            'user' => $user,
            'token' => $token
        ];
        return $response;
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
        return [
            'msg' => 'sukses'
        ];
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    //fungsi untuk menampilkan semua user
    public function index()
    {
        $users = DB::table('users')->select('id', 'name', 'email', 'is_admin', 'is_approved')->get();
        return response($users);
    }

    //fungsi untuk approve user baru
    public function approve(Request $request, $id)
    {
        $fields = $request->validate([
            'is_approved' => 'required|boolean'
        ]);
        if ($fields['is_approved']) {
            $user = DB::table('users')->where('id', $id)
                ->update(['is_approved', true]);
        }
        return response(['message' => 'success']);
    }

    //menjadikan anggota sebagai admin atau menjadikan admin sebagai anggota
    public function promote(Request $request, $id)
    {
        $fields = $request->validate([
            'is_admin' => 'required|boolean'
        ]);
        if ($fields['is_admin']) {
            $user = DB::table('users')->where('id', $id)
                ->update(['is_admin', true]);
        } else {
            $user = DB::table('users')->where('id', $id)
                ->update(['is_admin', true]);
        }
        return response(['message' => 'success']);
    }
}

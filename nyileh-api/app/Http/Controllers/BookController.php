<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    public function index()
    {
        $books = DB::table('books')
            ->join('locations', 'books.location_id', '=', 'locations.id')
            ->join('categories', 'books.location_id', '=', 'categories.id')
            ->select('books.*', 'locations.floor', 'categories.category')
            ->latest()->get();

        $response = [
            'msg' => 'sukses',
            'data' => $books
        ];

        return Response($response);
    }

    public function store(Request $request)
    {
        $fields = $request->validate([
            'category_id' => 'required|integer|exists:categories,id',
            'location_id' => 'required|integer|exists:locations,id',
            'title' => 'required|string',
            'description' => 'required|string',
            'cover' => 'required|string',
        ]);

        DB::table('books')->insert([
            'category_id' => $fields['category_id'],
            'location_id' => $fields['location_id'],
            'title' => $fields['title'],
            'description' => $fields['description'],
            'cover' => $fields['cover']
        ]);

        $response = [
            'message' => 'sukses'
        ];

        return Response($response, 201);
    }

    public function show($id)
    {
        $book = DB::table('books')
            ->join('locations', 'books.location_id', '=', 'locations.id')
            ->join('categories', 'books.location_id', '=', 'categories.id')
            ->select('books.*', 'locations.floor', 'categories.category')
            ->where('books.id', $id)->first();

        if ($book !== null) {
            $response = [
                'message' => 'sukses',
                'data' => $book
            ];

            return Response($response);
        }

        $response = [
            'message' => 'errors',
            'errrors' => [
                'Buku tidak ditemukan'
            ]
        ];

        return Response($response, 404);
    }

    public function update(Request $request, $id)
    {
        $book = DB::table('books')->where('id', $id)->first();

        if ($book !== null) {
            $fields = $request->validate([
                'category_id' => 'required|integer|exists:categories,id',
                'location_id' => 'required|integer|exists:locations,id',
                'title' => 'required|string',
                'description' => 'required|string',
                'cover' => 'required|string',
            ]);
            $book = DB::table('books')->where('id', $id)->update([
                'category_id' => $fields['category_id'],
                'location_id' => $fields['location_id'],
                'title' => $fields['title'],
                'description' => $fields['description'],
                'cover' => $fields['cover'],
            ]);
            $response = [
                'message' => 'sukses',
                'data' => $book
            ];
            return Response($response);
        }
        $response = [
            'message' => 'error',
            'errors' => [
                'Buku tidak terdaftar'
            ]
        ];
        return Response($response, 404);
    }

    public function destroy($id)
    {
        $book = DB::table('books')->where('id', $id)->first();
        if ($book !== null) {
            DB::table('books')->where('id', $id)->delete();
            $response = [
                'message' => 'sukses',
            ];
            return Response($response);
        }
        $response = [
            'message' => 'error',
            'errors' => [
                'Buku tidak terdaftar'
            ]
        ];
        return Response($response, 404);
    }
}

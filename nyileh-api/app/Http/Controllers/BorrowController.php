<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BorrowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borrows = DB::table('borrows')
            ->join('users', 'borrows.user_id', '=', 'users.id')
            ->select('borrows.*', 'users.name')
            ->get();

        return response($borrows);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->validate([
            'books' => 'required|array',
            'books.*' => 'required|integer|exists:books,id'
        ]);

        $errors = DB::transaction(function () use ($fields) {
            $errors = array();
            foreach ($fields['books'] as $book_id) {
                $book = DB::table('books')->where('id', $book_id)->first();
                if ($book->is_borrowed) {
                    array_push($errors, $book_id);
                }
            }

            if (count($errors) > 0) {
                return $errors;
            }


            $user_id = auth()->user()->id;
            $borrow_id = DB::table('borrows')->insertGetId([
                'user_id' => $user_id
            ]);

            foreach ($fields['books'] as $book_id) {
                DB::table('books')->where('id', $book_id)->update([
                    'is_borrowed' => true
                ]);
                DB::table('borrowed_books')->insert([
                    'borrow_id' => $borrow_id,
                    'book_id' => $book_id
                ]);
            }
        });

        if ($errors !== null) {
            return response([
                'message' => 'failed',
                'errors' => 'these books already borrowed',
                'books_id' => $errors
            ]);
        }
        return response(['message' => 'success'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $borrow = DB::table('borrows')
            ->join('users', 'borrows.user_id', '=', 'users.id')
            ->select('borrows.*', 'users.name')->where('borrows.id', $id)->get();

        return response($borrow);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = DB::transaction(function () use ($request, $id) {
            DB::table('borrows')->where('id', $id)->update(['is_approved', true]);
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

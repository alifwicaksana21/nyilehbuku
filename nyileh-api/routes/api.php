<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\BorrowController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::group(['middleware' => 'is_admin'], function () {
        Route::get('/user', [UserController::class, 'index']);
        Route::put('/user/{id}', [UserController::class, 'update']);

        Route::post('/book', [BookController::class, 'store']);
        Route::put('/book/{id}', [BookController::class, 'update']);
        Route::delete('/book/{id}', [BookController::class, 'destroy']);

        Route::post('/category', [CategoryController::class, 'store']);
        Route::get('/category/{id}', [CategoryController::class, 'show']);
        Route::delete('/category/{id}', [CategoryController::class, 'destroy']);

        Route::get('/borrow', [BorrowController::class, 'index']);
        Route::get('/borrow/{id}', [BorrowController::class, 'show']);
        Route::put('/borrow/{id}', [BorrowController::class, 'update']);
    });

    Route::post('/borrow', [BorrowController::class, 'store']);

    Route::get('/logout', [AuthController::class, 'logout']);
});

Route::get('/book', [BookController::class, 'index']);
Route::get('/book/{id}', [BookController::class, 'show']);

Route::get('/category', [CategoryController::class, 'index']);
